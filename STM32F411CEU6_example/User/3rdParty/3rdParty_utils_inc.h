/**
 * @file 3rdParty_utils_inc.h
 * @author 独霸一方 (2696652257@qq.com)
 * @brief //>这个头文件是用于包含3rdParty中所用到的头文件,这里面自动维护c\cpp头文件兼容,请不要将本头文件包含在 extern "C" 中
 * @version 1.0
 * @date 2022-09-21
 *
 * @copyright Copyright (c) 2022
 *
 */

// > 单次包含宏定义
#ifndef __3RDPARTY_UTILS_INC_H_
#define __3RDPARTY_UTILS_INC_H_

// > C/C++兼容性宏定义
#ifdef __cplusplus
extern "C"
{
#endif

    //+******************************** C包含 ***************************************/

#ifdef __cplusplus
}

//+******************************** C++ 包含 ***************************************/

//> 包含etl中需要使用的库
#include "./etl/queue.h"  //队列
#include "./etl/array.h"  //数组
#include "./etl/string.h" //字符串(其实没有用上)

//! 为了规范禁止在头文件中使用- using -关键字

#endif //\ __cplusplus

#endif //\ __3RDPARTY_UTILS_INC_H_
