/**
 * @file common_inc.h
 * @author 独霸一方 (2696652257@qq.com)
 * @brief //> 通用头文件包含,由于包含一些系统基础库以及底层依赖
 * @version 1.0
 * @date 2022-09-22
 *
 * @copyright Copyright (c) 2022
 *
 */

// > 单次包含宏定义
#ifndef __COMMON_INC_H_
#define __COMMON_INC_H_

//+******************************** 本头文件使用说明 ***************************************/
//!记得把这整个头文件放入freertos.c的包含部分,可以的话mian.c里面也包含一份
/**
 * @brief 在freeRTOS.c与mian.c包含此头文件,然后将创建的虚线程函数入口全部重写即可
 * 这样做是为了减少与自动创建的代码之间的耦合
 */

// > C/C++兼容性宏定义
#ifdef __cplusplus
extern "C"
{
#endif
    //+******************************** C语言规则编译部分 ***************************************/
    //> 对于整个工程来说,整个工程都由CubeMX生成,目前CubeMX自动生成的文件是已经做好C++环境的兼容(FreeRTOS本体以及底层驱动库都是C语言)

#include "stm32f4xx_hal.h"
#include "FreeRTOS.h"
#include "portable.h"
#include "semphr.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"
#include "dma.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"

    //+******************************** 外部函数重写声明等 ***************************************/
    //> 重写在CubeMX中配置的两个任务入口函数(CubeMX中被声明为了弱函数,位于freertos.c中)
    extern void TestServer(void *argument);
    extern void ShellServer(void *argument);
    // extern void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);
    extern void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size); // DMA串口事件回调函数

#ifdef __cplusplus
}

//+******************************** C++规则编译部分 ***************************************/
//> 一般来说默认User中都是C++实现,但是其中实在有需要用C语言规则编译部分,请在对应的头文件中实现包含规范

//> 第三方库包含
#include "../3rdParty/3rdParty_utils_inc.h"

//> 用户库包含
#include "../lib/lib_utils_inc.h"

#endif //\ __cplusplus

#endif //\ __COMMON_INC_H_
