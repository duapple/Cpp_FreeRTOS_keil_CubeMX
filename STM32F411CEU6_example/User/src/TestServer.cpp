/**
 * @file TestServer.cpp
 * @author 独霸一方 (2696652257@qq.com)
 * @brief
 * @version 1.0
 * @date 2022-09-18
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "common_inc.h"

void TestServer(void *argument)
{
    /* init code for USB_DEVICE */
    MX_USB_DEVICE_Init(); //> USB初始化,暂未使用
    /* USER CODE BEGIN TestServer */
    /* Infinite loop */
    for (;;)
    {
        HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin); //翻转LED电平,闪烁
        osDelay(1000);

        //使用log输出日志,log对象创建时默认是输出所有等级信息,控制台中可以通过调用 logSetLevel (等级枚举的等效数字) 来设置只输出那些内容,日志等级枚举在log.h中 LogLevel
        logError("this is error info!");
        logDebug("this is debug info!");
        //> log断言,第一个参数是断言表达式,第二个是断言失败之后执行的操作,(void)0表示什么都不做,这是在FreeRTOS任务死循环中运行,这里别改成return了
        logAssert((0 == 1), (void)0); //> 使用日志执行断言,断言失败会通过log输出详细信息,文件名及行数以及表达式,断言失败的日志输出等级是错误级(同logError输出)
    }
    /* USER CODE END TestServer */
}
