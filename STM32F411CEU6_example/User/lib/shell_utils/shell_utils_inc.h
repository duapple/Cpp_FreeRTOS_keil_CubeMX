/**
 * @file shell_utils_inc.h
 * @author 独霸一方 (2696652257@qq.com)
 * @brief //>这个头文件是为了解决shell中的有效包含问题,所有需要用到的shell的地方请包含此头文件而避免直接包含根目录头文件
 *  //!包含此头文件时不要包含在 extern "C" 中!!!
 * @version 1.0
 * @date 2022-09-21
 *
 * @copyright Copyright (c) 2022
 *
 */

// > 单次包含宏定义
#ifndef __SHELL_UTILS_INC_H_
#define __SHELL_UTILS_INC_H_

// > C/C++兼容性宏定义
#ifdef __cplusplus
extern "C"
{
#endif

    //+******************************** C包含 ***************************************/

//> 移植文件包含
#include "./transplant/shell_cfg.h"

//> shell本体包含
#include "./shell_base/shell.h"
#include "./shell_base/shell_ext.h"

//> 移植文件包含,这个要放在后面
#include "./transplant/shell_port.h"

//> log日志包含
#include "./log/log.h"

//> shell增强包含
#include "./shell_enhance/shell_cmd_group.h"
#include "./shell_enhance/shell_passthrough.h"
#include "./shell_enhance/shell_secure_user.h"

    //> shell游戏包含,游戏包含并没有头文件需要包含,请把对应的.c文件加入编译

#ifdef __cplusplus
}
#endif //\ __cplusplus

//+******************************** C++包含 ***************************************/

//> 包含通用头文件
#include "common_inc.h"

//> shell C++支持包含,上面已经做好了C包含的处理
//> 移植文件中包含
#include "./transplant/shell_cpp.h"
#include "./transplant/shell_utils.hpp"

#endif //\ __SHELL_UTILS_INC_H_
