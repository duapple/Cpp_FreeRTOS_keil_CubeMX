/**
 * @file userCMD.h
 * @author 独霸一方 (2696652257@qq.com)
 * @brief //> 用户个人指令添加
 * @version 1.0
 * @date 2022-09-21
 *
 * @copyright Copyright (c) 2022
 *
 */

// > 单次包含宏定义
#ifndef __USERCMD_H_
#define __USERCMD_H_

#include "common_inc.h"
#include "../shell_utils_inc.h"

// > C/C++兼容性宏定义
#ifdef __cplusplus
extern "C"
{
#endif

    //> 添加软重启
    int Restart(int argc, char *agrv[])
    {
        HAL_NVIC_SystemReset(); //单片机重启
    }
    SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0) | SHELL_CMD_TYPE(SHELL_TYPE_CMD_MAIN), Restart, Restart, restart MCU);

#ifdef __cplusplus
}
#endif //\ __cplusplus

#endif //\ __USERCMD_H_
