/**
 * @file shell_port.h
 * @author Letter (NevermindZZT@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-02-22
 *
 * @copyright (c) 2019 Letter
 *
 */

#ifndef __SHELL_PORT_H__
#define __SHELL_PORT_H__

#include "../shell_utils_inc.h"

// > C/C++兼容性包含
#ifdef __cplusplus
extern "C"
{
#endif

    extern Shell shell;

    void userShellInit(void);

#ifdef __cplusplus
}
#endif //\ __cplusplus

#endif
