/**
 * @file shell_port.c
 * @author Letter (NevermindZZT@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-02-22
 *
 * @copyright (c) 2019 Letter
 *
 */

#include "../shell_utils_inc.h"

// > C/C++兼容性宏定义
#ifdef __cplusplus
extern "C"
{
#endif

    Shell shell;
    char shellBuffer[256];
    static SemaphoreHandle_t shellMutex; //互斥锁,防止输出混乱

    /**
     * @brief 用户shell写
     *
     * @param data 数据
     * @param len 数据长度
     *
     * @return short 实际写入的数据长度
     */
    short userShellWrite(char *data, unsigned short len)
    {
        HAL_UART_Transmit(&huart1, (uint8_t *)data, len, HAL_MAX_DELAY); //无限期等下去
        return len;
    }

    /**
     * @brief 用户shell上锁
     *
     * @param shell shell
     *
     * @return int 0
     */
    int userShellLock(Shell *shell)
    {
        xSemaphoreTakeRecursive(shellMutex, portMAX_DELAY);
        return 0;
    }

    /**
     * @brief 用户shell解锁
     *
     * @param shell shell
     *
     * @return int 0
     */
    int userShellUnlock(Shell *shell)
    {
        xSemaphoreGiveRecursive(shellMutex);
        return 0;
    }

    /**
     * @brief 用户shell初始化
     *
     */
    void userShellInit(void)
    {

        shell.write = userShellWrite;
        // shell.read = userShellRead;//> 暂未添加shell读取函数,放在cpp环境中去绑定cpp环境中的函数

        //>添加互斥锁
        shellMutex = xSemaphoreCreateMutex();
        shell.lock = userShellLock;
        shell.unlock = userShellUnlock;

        shellInit(&shell, shellBuffer, sizeof(shellBuffer));
    }

#ifdef __cplusplus
}
#endif //\ __cplusplus
