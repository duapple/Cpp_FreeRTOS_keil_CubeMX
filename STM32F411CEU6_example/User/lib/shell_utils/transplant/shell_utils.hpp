/**
 * @file shell_utils.hpp
 * @author 独霸一方 (2696652257@qq.com)
 * @brief //> shell的C++环境初始话头文件包含
 * @version 1.0
 * @date 2022-09-21
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "../shell_utils_inc.h"

// > 单次包含宏定义
#ifndef __SHELL_UTILS_H_
#define __SHELL_UTILS_H_

// > C/C++兼容性宏定义
#ifdef __cplusplus

//+******************************** 接收DMA接收缓冲区以及先入先出队列 ***************************************/
extern etl::array<uint8_t, 100> *arrayBuffer; //> etl数组指针

//!这里只是声明,在对应的cpp文件中完成创建以及初始化
extern etl::queue<uint8_t, 100> *fifoArray; //> 创建在RTOS堆上,运行期间永不释放就不存在内存碎片化问题

void userShellInit_cpp(void); // C++环境初始化shell

//+******************************** log日志输出 ***************************************/
//声明日志对象
extern Log myLog;

void userLogInit_cpp(void); // C++环境初始化log日志输出

#endif //\ __cplusplus

#endif //\ __SHELL_UTILS_H_
