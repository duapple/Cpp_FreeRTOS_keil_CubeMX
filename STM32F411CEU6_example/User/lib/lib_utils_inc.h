/**
 * @file lib_utils_inc.h
 * @author 独霸一方 (2696652257@qq.com)
 * @brief //>这个头文件是用于包含lib中所用到的头文件,这里面自动维护c\cpp头文件兼容,请不要将本头文件放入 extern "C" 中
 * @version 1.0
 * @date 2022-09-21
 *
 * @copyright Copyright (c) 2022
 *
 */

// > 单次包含宏定义
#ifndef __LIB_UTILS_INC_H_
#define __LIB_UTILS_INC_H_

// > C/C++兼容性宏定义
#ifdef __cplusplus
extern "C"
{
#endif

    //+******************************** C包含 ***************************************/

#ifdef __cplusplus
}

//+******************************** C++ 包含 ***************************************/

//> 控制台头文件包含
#include "./shell_utils/shell_utils_inc.h"

#endif //\ __cplusplus

#endif //\ __LIB_UTILS_INC_H_
